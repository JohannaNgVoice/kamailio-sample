# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Prerequisites
You need to have docker-compose installed with the following version
```
docker-compose version 1.27.4
```
* Configuration
In the docker-compose.yaml file adapt the following

Replace 192.168.1.150 with the IPv4 address of the machine where you want to start the docker container.

* Start
To start run the following command in sample_docker_compose folder

```
source .env
docker-compose build
docker-compose up
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact