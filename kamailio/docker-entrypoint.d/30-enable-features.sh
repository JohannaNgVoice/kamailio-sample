#!/usr/bin/env sh
#
#
# copyright   : 2020 ng-voice GmbH
# author      : Rick Barenthin <rick@ng-voice.com>
# date        : 2020-12-15
#
# description : This script will configure the feature parameters based on the
#               input environment variables
#
# env vars    : CAPTURE_NODE, WITH_INBOUND_REG, WITH_XHTTP_PROM,
#               WITH_SUBSCRIBE_RECORDROUTE, WITH_JSONRPC, MONITORING_PORT,
#               WITH_RESTORATION
#
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# SAMPLE Kamailio config file path
#------------------------------------------------------------------------------
SAMPLE_CONFIG="$BASEDIR/sample.cfg"

#------------------------------------------------------------------------------
# Set default listen parameters

# Environment variables will override these default values
#------------------------------------------------------------------------------
WITH_DEBUGGING=${WITH_DEBUGGING:-"0"}


if [ "$WITH_DEBUGGING" -eq 1 ]; then
  echo "Debugging: Enabled"
    sed -i -e "s/##!define WITH_DEBUGGING/#!define WITH_DEBUGGING/g" "$SAMPLE_CONFIG"
fi