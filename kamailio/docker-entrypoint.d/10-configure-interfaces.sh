#!/usr/bin/env sh
#
#
# copyright   : 2022 ng-voice GmbH
# author      : Johanna Lämmel <johanna@ng-voice.com>
# date        : 2022-01-19
#
# description : This script will configure the listen parameters based on the
#               input environment variables
#
# env vars    : NODE_ID, DOMAINS, EXTERNAL_IPV4, EXTERNAL_IPV6,
#               WITH_IPV4, WITH_IPV6, EXTRA_ALIASES, WITH_TCP,
#               TCP_CON_LIFETIME
#
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Sample Kamailio config file path
#------------------------------------------------------------------------------
SAMPLE_CONFIG="$BASEDIR/sample.cfg"

#------------------------------------------------------------------------------
# Set default listen parameters

# Environment variables will override these default values
#------------------------------------------------------------------------------
NODE_ID=${NODE_ID:-"1"}
DOMAINS=${DOMAINS:-"mnc001.mcc001.3gppnetwork.org"}
EXTERNAL_IPV4=${EXTERNAL_IPV4:-""}
EXTERNAL_IPV6=${EXTERNAL_IPV6:-""}
WITH_IPV4=${WITH_IPV4:-"1"}
WITH_IPV6=${WITH_IPV6:-"0"}
EXTRA_ALIASES=${EXTRA_ALIASES:-""}
WITH_TCP=${WITH_TCP:-"1"}
TCP_CON_LIFETIME=${TCP_CON_LIFETIME:-"0"}

if [ -z "$NODE_ID" ]; then
  echo "Incorrect value '$NODE_ID' passed to 'NODE_ID' env variable."
  echo "This is a mandantory env variable which can not be empty"
  echo "Exiting..."
  exit 1
fi

if [ -z "$DOMAINS" ]; then
  echo "Incorrect value '$DOMAINS' passed to 'DOMAINS' env variable."
  echo "This is a mandantory env variable which can not be empty"
  echo "Exiting..."
  exit 1
fi

if [ "$WITH_IPV4" -eq 0 ] && [ "$WITH_IPV6" -eq 0 ]; then
  echo "Incorrect value passed to 'WITH_IPV4' and 'WITH_IPV6' env variable."
  echo "The can not be both set to 0"
  echo "Exiting..."
  exit 1
fi

if [ "$WITH_IPV4" -eq 1 ]; then
  echo "IPv4: Enabled"
  if [ -z "$EXTERNAL_IPV4" ]; then
    echo "Incorrect value '$EXTERNAL_IPV4' passed to 'EXTERNAL_IPV4' env variable."
    echo "This is a mandantory env variable if you enable IPv4 and can not be empty"
    echo "Exiting..."
    exit 1
  fi

  echo "External IPv4 to use is $EXTERNAL_IPV4"
  sed -i -e "s/##!substdef \"!EXTERNAL_IPV4!EXTERNAL_IPV4!g\"/#!substdef \"!EXTERNAL_IPV4!$EXTERNAL_IPV4!g\"/g" "$SAMPLE_CONFIG"
elif [ "$WITH_IPV4" -eq 0 ]; then
  echo "IPv4: Disabled"
else
  echo "Incorrect value '$WITH_IPV4' passed to 'WITH_IPV4' env variable."
  echo "Supported options: 0 or 1"
  echo "Exiting..."
  exit 1
fi

if [ "$WITH_IPV6" -eq 1 ]; then
  echo "IPv6: Enabled"
  if [ -z "$EXTERNAL_IPV6" ]; then
    echo "Incorrect value '$EXTERNAL_IPV6' passed to 'EXTERNAL_IPV6' env variable."
    echo "This is a mandantory env variable if you enable IPv6 and can not be empty"
    echo "Exiting..."
    exit 1
  fi

  echo "External IPv6 to use is $EXTERNAL_IPV6"
  sed -i -e "s/##!substdef \"!EXTERNAL_IPV6!EXTERNAL_IPV6!g\"/#!substdef \"!EXTERNAL_IPV6!$EXTERNAL_IPV6!g\"/g" "$SAMPLE_CONFIG"
elif [ "$WITH_IPV6" -eq 0 ]; then
  echo "IPv6: Disabled"
else
  echo "Incorrect value '$WITH_IPV6' passed to 'WITH_IPV6' env variable."
  echo "Supported options: 0 or 1"
  echo "Exiting..."
  exit 1
fi

if [ "$WITH_TCP" -eq 1 ]; then
  echo "TCP: Enabled"
  sed -i -e "s/##!define WITH_TCP/#!define WITH_TCP/g" "$SAMPLE_CONFIG"
elif [ "$WITH_TCP" -eq 0 ]; then
  echo "TCP: Disabled"
else
  echo "Incorrect value '$WITH_TCP' passed to 'WITH_TCP' env variable."
  echo "Supported options: 0 or 1"
  echo "Exiting..."
  exit 1
fi

PRIMARY_DOMAIN=""
ALIAS=""
for DOMAIN in $DOMAINS; do
  if [ -z "$PRIMARY_DOMAIN" ]; then
    PRIMARY_DOMAIN=$DOMAIN
  fi

  ALIAS="$ALIAS\nalias=ims.${DOMAIN}"

  if [ "$WITH_IPV4" -eq 1 ]; then
    ALIAS="$ALIAS\nalias=EXTERNAL_IPV4:4060"
  fi

  if [ "$WITH_IPV6" -eq 1 ]; then
    ALIAS="$ALIAS\nalias=EXTERNAL_IPV6:4060"
  fi

  ALIAS="$ALIAS\nalias=sample.${DOMAIN}"

  ALIAS="$ALIAS\nalias=sample-${NODE_ID}.${DOMAIN}"

  for ALIAS_I in $EXTRA_ALIASES; do
    ALIAS="$ALIAS\nalias=sample-${ALIAS_I}.${DOMAIN}"
  done
done

if [ -z "$PRIMARY_DOMAIN" ]; then
  echo "Incorrect value '$DOMAINS' passed to 'DOMAINS' env variable."
  echo "This is a mandantory env variable and can not be empty"
  echo "Exiting..."
  exit 1
fi

if [ -n "$TCP_CON_LIFETIME" ]; then
  sed -i -e "s/#!define TCP_CON_LIFETIME 3615/#!define TCP_CON_LIFETIME $TCP_CON_LIFETIME/g" "$SAMPLE_CONFIG"
fi

sed -i -e "s/advertise sample\.mncXXX\.mccXXX\.3gppnetwork\.org:4060/advertise sample-$NODE_ID.$PRIMARY_DOMAIN:4060/g" "$SAMPLE_CONFIG"
sed -i -e "s/alias=ims\.mncXXX\.mccXXX\.3gppnetwork\.org/$ALIAS/g" "$SAMPLE_CONFIG"

sed -i -e "s/#!define NETWORKNAME \"ims\.mncXXX\.mccXXX\.3gppnetwork\.org\"/#!define NETWORKNAME \"ims.$PRIMARY_DOMAIN\"/g" "$SAMPLE_CONFIG"
sed -i -e "s/#!define INBOUND_URI \"ims\.mncXXX\.mccXXX\.3gppnetwork\.org:4060\"/#!define INBOUND_URI \"ims.$PRIMARY_DOMAIN:4060\"/g" "$SAMPLE_CONFIG"
