# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Preparation for IPv6 ###

## Pre-requisites

This repository makes use of IPv6 so it must be enabled as follows:

* Enable IPv6 at host machine level

```
sudo su
echo "net.ipv6.conf.default.disable_ipv6 = 0" >> /etc/sysctl.conf
echo "net.ipv6.conf.all.disable_ipv6 = 0" >> /etc/sysctl.conf
echo "net.ipv6.conf.all.forwarding = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.default.forwarding = 1" >> /etc/sysctl.conf
sysctl -p
```

* Enable IPv6 at Docker Daemon level by editing /etc/docker/daemon.json file (create if it does not exist).

```
{
	"ipv6": true,
	"ip6tables": true,
	"experimental": true,
	"fixed-cidr-v6": "2001:3984:3989::/64"
}
```

* Load IPv6 kernel module.

```
sudo modprobe ip6table_filter
```

Then, restart docker service

```
sudo systemctl restart docker
```

* Enable IPv6 NAT for the subnet used for Docker

```
sudo ip6tables -t nat -A POSTROUTING -s  2001:3984:3989::/64 ! -o docker0 -j MASQUERADE
```

## Setup FHoSS

### Download
```
git clone git@bitbucket.org:ngvoice/fhoss.git
Configuration
```

### Modify the following parameters as per your deployment in .env file where,

MCC = Mobile Country Code of your network

MNC = Mobile Network Code of your network

EXT_DNS_IP = IPv6 for external DNS to contact (DNS of IMS deployment)

DOCKER_HOST_IP = Machine/VM IPv6 address where FHoSS Docker setup is running (e.g. ens3/eno1/eth0 IPv6 address)

SCSCF_PEER = S-CSCF diameter identity without domain e.g. scscf-1
SCSCF_PORT = Diameter Port S-CSCF is listening on

ICSCF_PEER = I-CSCF diameter identity without domain e.g. icscf-1
ICSCF_PORT = Diameter Port I-CSCF is listening on

Note: ICSCF_IP and SCSCF_IP is for the DNS which is currently not working properly


FHOSS_PORT = Diameter Port on which FHoSS is listening for Diameter connections

### Compile
```
cd fhoss
source .env
docker-compose build
```

### Run
```
cd fhoss
source .env
docker-compose up
```

 


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact