Feature: An example Feature

  Scenario: SIP REGISTER
    Given a User Agent

    When the User Agent sends a SIP REGISTER 'without_credentials' request to the Sample Kamailio (SUT)
    Then the Sample Kamailio (SUT) responds with a SIP 'everything_alright' with status 200 OK to the 'without_credentials' request sent by the User Agent

  Scenario: SIP INVITE
    Given a User Agent

    When the User Agent sends a SIP INVITE 'establish_call' request to the Sample Kamailio (SUT)
    Then the Sample Kamailio (SUT) responds with a SIP 'dont_call_me' with status 405 Method Not Allowed to the 'establish_call' request sent by the User Agent