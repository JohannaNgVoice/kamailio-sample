"""
Initialize Pinctada.

Author: Matthias Bräuer <matthias@ng-voice.com>
Copyright: 2021 ng-voice GmbH
"""

# Load all pytest fixtures and pytest cli options from Pinctada.
pytest_plugins = ['pinctada.init']
