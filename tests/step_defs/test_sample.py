"""
Author: Matthias Bräuer <matthias@ng-voice.com>
Copyright: 2021 ng-voice GmbH
"""
from functools import partial

from pytest_bdd import scenario

scenario = partial(scenario, 'features/sample.feature')


@scenario("SIP REGISTER")
def test_sip_register():
    pass


@scenario("SIP INVITE")
def test_sip_invite():
    pass
