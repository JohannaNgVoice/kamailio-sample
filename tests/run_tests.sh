#!/usr/bin/env sh

docker exec -ti pinctada python -m \
       pytest -o log_cli=1 -o log_cli_level=INFO -o bdd_features_base_dir=sut-tests/tests/ \
       --disable-warnings --gherkin-terminal-reporter --gherkin-terminal-reporter-expanded \
       --config-dir=sut-tests/tests/ /home/diver/sut-tests/tests/step_defs/