#!/bin/sh

IMAGE_NAME=dockerhub.ng-voice.com/kamailio-sample:$(git branch | grep \* | cut -d ' ' -f2 | tr '/' '_')
docker build -t $IMAGE_NAME .